\documentclass[12pt,a4paper]{report}

\usepackage{datetime}
\usepackage{graphicx}
\usepackage{mathptmx}
\usepackage[
  top=2cm,
  left=3cm,
  right=6cm,
  bottom=2cm
%  margin=2cm            % 2cm page margins, for final document
  ]{geometry}           % alter document geometry
\usepackage{lscape}

% tables etc
\usepackage{csvsimple}
\usepackage{datatool}
\usepackage{longtable}
\usepackage{multirow}   % for \multirow in tables

\usepackage{titlesec}
\usepackage{fontspec}
\usepackage[
  textsize=scriptsize,
  textwidth=4cm
  ]{todonotes}          % for the \todo and associated commands


% Code etc
\usepackage{listings}   % for code listings
\usepackage{minted}     % for pretty code


\usepackage{setspace}   % for line spacings etc
\doublespacing          % set line spacings to doublespaced

\usepackage[
  labelfont=bf,         % bold for the ``Figure X'' bit
  footnotesize,         % smaller
  hypcap                % allow hyperlinks
  ]{caption}            % customise captions

%bib/citations
\usepackage[
  hyperref=true,
  uniquename=false,
  uniquelist=false,
  backend=biber,
  style=authoryear,
  bibstyle=rsbthesis,
  sortcites=true,
  backref=true,
  maxcitenames=3,
  maxbibnames=20,
  url=false,
  isbn=false
  ]{biblatex}
\addbibresource{thesis.bib}

\usepackage{blindtext}                      % for lorem ipsum text. remove eventually

% hyperlinks
\usepackage{hyperref}                       % Keep hyperref as the last package

\hypersetup{
  bookmarks=false,                          % show bookmarks bar?
  unicode=true,                             % non-Latin characters in Acrobat’s bookmarks
  pdftoolbar=true,                          % show Acrobat’s toolbar?
  pdfmenubar=true,                          % show Acrobat’s menu?
  pdffitwindow=false,                       % window fit to page when opened
  pdfstartview={FitW},                      % fits the width of the page to the window
  pdftitle={Kevin Murray Honours Thesis},   % title
  pdfauthor={Kevin Murray},                 % author
  pdfsubject={},                            % subject of the document
  pdfcreator={},                            % creator of the document
  pdfproducer={},                           % producer of the document
  pdfkeywords={}{}{},                       % list of keywords
  pdfnewwindow=true,                        % links in new window
  colorlinks=true,                          % false: boxed links; true: colored links
  linkcolor=red,                            % color of internal links (change box color with linkbordercolor)
  citecolor=green,                          % color of links to bibliography
  filecolor=magenta,                        % color of file links
  urlcolor=cyan}                            % color of external links

\usepackage[all]{hypcap}

%%%%%%%%%%%%%%%%%%%%% global commands %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\pagestyle{headings}    % activate Chapter heading


%\titleformat{\chapter}  % title format
%  [hang]
%  {\bf\huge}
%  {\thechapter}
%  {2pc}
%  {}
%

%\providecommand*{\listingautorefname}{Listing} % allow autoref of \label{lst:...}. see:
% http://tex.stackexchange.com/questions/13760/use-autoref-with-minted-and-its-listing-environment


\input{macros.latex}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%                      Begin Document                               %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\begin{titlepage}
  \begin{center}
    \null
    \vfill
    \textsc{\Large Gene expression variation under dynamic light in
      \textit{Arabidopsis thaliana}}\\[1.5cm]
    \textit{\Large Kevin Murray\\Borevitz Lab, ANU\\[1.5cm]}
    \today
    \vfill
    \small Thesis submitted in partial fufillment of the requirements of the degree of\\
    \small Bachelor of Philosophy (Science) (Honours)\\
    \vfill
    \footnotesize Word Counts:\\
    \begin{tabular}{ll}
      \footnotesize Introduction: &  x words\\
      \footnotesize Results: & y words\\
      \footnotesize Discussion: & z words\\
    \end{tabular}
  \end{center}
\end{titlepage}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%                       Abstract                                %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{abstract}
  This is the abstract.
  \blindtext[2] % remove this in reality
\end{abstract}

\tableofcontents

\todototoc
\listoftodos

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%                          Todo list                                %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Todo List}

\todo{Tim: images from Col, Cvi, Ler from all timepoints}
\todo{Keng: Exact details of planting times}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%                          Imported Chapters                        %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{intro.latex}

\input{dynamiccond.latex}

\input{betterrnaseq.latex}

\input{txomevar.latex}

\input{discussion.latex}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%                      Bibliography                                 %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\printbibliography


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%                        Appendix                                   %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Appendix}

\centerline{
  \begin{minipage}[t]{0.8\textwidth}
    \centering
    Notes:
    \begin{itemize}
      \item Code listings, where included, are illustrative. Full source code of all software
        developed is large (over 5000 lines of code), and will be distrubuted as a gzipped tar
        archive. The latest code for all pipleines, scripts, is available online. See Appendix
        \autoref{sec:appendix-sourcecoderepos} and \autoref{tab:appendix-code-repos}
    \end{itemize}
  \end{minipage}
}

\clearpage
\input{appendices/coderepos.latex}

\clearpage
\input{appendices/spcControl.latex}

\end{document}
